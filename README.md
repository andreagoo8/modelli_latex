# Modelli per Latex

Modelli di sorgenti per Latex e Lualatex pronti per l'uso: contengono i pacchetti principali e alcuni utili commentati.

* `Modello_article_Latex-f.tex` versione contenente tutti i pacchetti con le relative impostazioni commentate e spiegate
* `mal-e-standalone.tex` versione contenente solo i pacchetti essenziali, e con il preambolo nello stesso file sorgente del documento root
* `source-mal-e.tex` e `preamble-mal-e.tex` versione contenente solo i pacchetti essenziali, e con il preambolo in un file sorgente diverso da quello del documento root, a cui è collegato con un `input{}`
