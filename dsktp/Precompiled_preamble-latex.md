# Usando pdflatex
## Separazione sorgenti
Il documento contenente la parte statica (`preamble.tex`) deve contenere tutto da `\documentclass` a `\usepackage{bookmark}`. L'ultima riga deve essere il comando `\endofdump`.

Nel documento contenente la parte dinamica (`main.tex`):
* la prima riga deve essere `%&<formatname>`, che specifica quale file di formato usare. Se si include questa indicazione anche nel comando per la creazione del pdf, questa riga non crea errori. Se si usa Lualatex questa riga non viene riconosciuta.
* la seconda riga deve essere `\endofdump`; a volte funziona anche senza includerla nel main, ma includendola si possono inserire, sotto di essa, alcuni pacchetti e comandi che non funzionano se vengono precompilati (per esempio, `xcolor`, se usato nel `preamble.tex`, riesce a impostare correttamente il colore della pagina ma non quello del font principale).
* a seguire ci va tutto il resto.

## Creazione format
```
pdflatex -ini -jobname=<formatname> "&pdflatex" mylatexformat.ltx preamble.tex
```
* `-jobname=<formatname>` indica il nome da dare al file `.fmt`
* `"&pdflatex"` è necessario; senza di lui né `pdftex` né `pdflatex` funzionano
* `mylatexformat.ltx` è "l'eseguibile" del pacchetto, preso da `/usr/share/texmf-dist/tex/latex/mylatexformat/`
* crea un file `<formatname>.fmt`, e questo file:
	* ha dimensioni esatte uguali ad uno creato con `pdftex`
	* ha sha256sum diverso da uno creato con `pdftex` o `pdflatex` immediatamente dopo


## Creazione pdf
```
pdflatex "&<formatname>" main.tex
```
* crea il pdf, e questo pdf è uguale ad uno creato con `pdftex` (ma ha sha256sum diverso)
* `"&<formatname>"` indica il file `.fmt` che deve essere usato
* se `%&<formatname>` è già presente come prima riga di `main.tex` non serve includerlo anche nel comando. Funziona anche con Texstudio


# Usando Lualatex
Creazione format:
```
luahbtex -ini -jobname=<formatname> "&lualatex" mylatexformat.ltx preamble.tex
```

Creazione pdf:
```
luahbtex "&a" main.tex
```

Usando Lualatex con il modello article full e provando ad usare un formato con il pacchetto precompilato, i pacchetti che danno errore in fase di compilazione sono `polyglossia`, `microtype`, `siunitx`, `chemformula`. Poi non è detto che gli altri pacchetti, pur non producendo errori, funzionino davvero: per esempio in alcune prove con biblatex, anche semplici, non sono riuscito neanche a compilare.

# Texstudio
## Pdflatex
Il comando `pdflatex -ini -jobname=preamble "&pdflatex" mylatexformat.ltx preamble.tex` è configurato come shortcut F6. Il main poi può comodamente essere compilato con i comandi soliti aggiungendo all'inizio del file la stringa `"&<formatname>"`.

## Lualatex
Si può configurare la stringa per la creazione format, ma poi è scomoda la creazione del pdf perché bisogna creare anche un comando + shortcut apposito che contenga l'indicazione del format da usare.

# Note
I file .fmt pesano non poco (relativamente alla media dei file latex): circa 11/12 MB, anche quelli semplici.

# TODO
[Altra domanda su TexSX da consultare per lualatex](https://tex.stackexchange.com/questions/324559/guidelines-for-using-mylatexformat-with-luatex/521482#521482)

# Bibliografia
[Ultrafast PDFLaTeX with precompiling](https://tex.stackexchange.com/questions/79493/ultrafast-pdflatex-with-precompiling)
[mylatexformat package](https://ctan.mirror.garr.it/mirrors/ctan/macros/latex/contrib/mylatexformat/mylatexformat.pdf)
