# Note subcaption
Il sostituto del comando `\subfloat` è l'ambiente `subcaptionblock`, che _makes a box with given width_. È molto simile ad un ambiente _minipage_ e forse, sotto il cofano, è proprio lo stesso... ma non sono sicuro. In ogni caso le opzioni sono le stesse di _minipage_, quindi ecco la loro descrizione in ordine di "obbligatorietà":
```
\begin{minipage}[<outer-pos>][<height>][<inner-pos>]{<width>}
\end{minipage}
```
* `<width>` indica la larghezza della box
* `<outer-pos>` indica il posizionamento verticale della box stessa rispetto alla linea di testo esterna all'ambiente _subcaptionblock_
* `<height>` indica l'altezza della box
* `<inner-pos>` indica il posizionamento verticale del contenuto della box rispetto ai margini della box stessa

Siccome `subcaptionblock` solito viene inserito in un ambiente _figure_, dentro al quale solitamente non ha senso inserire testo, l'unica opzione che sembra aver senso usare è `<width>`, che regola

```
\begin{subcaptionblock}[<outer-pos>][<height>][<inner-pos>]{<width>}
\end{subcaptionblock}
```

Il `label` lascialo dov'è altrimenti c'è uno spazio indesiderato






Il comando precedente va inserito in un ambiente figure ogni volta che si inserisce una sottofigura, in modo che tutto diventi:
```
\begin{figure}[H]
\centering
	\subcaptionbox{Caption for figure 1\_1\label{fig:1_1}}[width_fig1_1]
		{\includegraphics{filename1_1}}
	\subcaptionbox{Caption for figure 1\_2\label{fig:1_2}}[width_fig1_2]
		{\includegraphics{filename1_2}}
\caption{Caption for figure 1}\label{fig:1}
\end{figure}
```

