# Note glossaries
* `\cmd*{}` non produce un hyperlink al glossario
* `\glssymbol{}` mostra ciò che nel `\glossaryentry` è sotto il campo `symbol`
* Nella section 4 spiega 4 modi diversi di sortare le entries per la stampa del glossario. In base a quella scelta si può anche specificare quale tipo di entries stampare (se sono state divise per tipo)
* Di default glossaries aggiunge un punto fermo alla fine della description, mentre glossaries-extra no. Per aggiungerlo se usi glossaries scrivi `nopostdot` nelle opzioni di `\usepackage`, per toglierlo se usi glossaries-extra scrivi `postdot`.
* Spesso alla fine della descrizione di una entry nel glossario viene stampata una lista delle pagine in cui quella entry compare (a volte, in base a vari casi e modalità di produzione del glossario, questo potrebbe non succedere: maggiori dettagli nei primi paragrafi della section 5). Per evitare questa cosa scrivi `nonumberlist` nelle opzioni di `\usepackage`.
* Entries are grouped according to the first letter of each entry’s sort key. By default a vertical gap is placed between letter groups for most of the predefined styles. You can suppress this with `\usepackage[nogroupskip]{glossaries}`
* The glossaries package predefines a default main glossary. When you define an entry (using one of the commands described in section 2), that entry is automatically assigned to the default glossary, unless you indicate otherwise using the type key. However you first need to make sure the desired glossary has been defined. This is done using `\newglossary[<log-ext>]{<glossary-label>}{<out-ext>}{<in-ext>}{<title>}`
* The package option `acronyms` is a is a convenient shortcut for `\newglossary[alg]{acronym}{acr}{acn}{\acronymname}`. The option also changes the behaviour of `\newacronym` so that acronyms are automatically put in the list of acronyms instead of the main glossary. For glossaries-extra, is the same but with the option `abbreviations`
* If you want to disable this for all your glossaries, then use`\glsdisablehyper`, or the option `nohypertypes={type1,type2}`. `hyperfirst=false` disables the hyperlink for the first entry
* Don't use commands like `\gls` in a section heading; instead use `\glsentrytext{}` per glossaries e `\glsfmttext{}` per glossaries-extra
* Con `see={label}` puoi mettere una reference nel glossario ad un'altra reference del glossario.

> The default glossary style uses the description environment to display the entry list. Each entry name is set in the optional argument of \item which means that it will typically be displayed in bold. Some classes and packages redefine the description environment in such as way that’s incompatible with the glossaries package. In which case you’ll need to select a different glossary style.
